package main.java.MyGame;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.event.MouseInputListener;
import javax.swing.plaf.basic.BasicArrowButton;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by Void on 31.07.2014.
 */
public class MapCreator extends JFrame implements MouseMotionListener{
    public void mouseDragged(MouseEvent e) {
        int b1 = MouseEvent.BUTTON1_DOWN_MASK;
        int b3 = MouseEvent.BUTTON3_DOWN_MASK;
        if ((e.getModifiersEx() & (b1 | b3)) == b1) {
            int pointX = (int) (e.getX() / 50) * 50;
            int pointY = (int) (e.getY() / 50) * 50;
            currentX = pointX / 50 + sideX;
            currentY = pointY / 50 + sideY;
            leftButtonAction();

        }else if((e.getModifiersEx() & (b1 | b3)) == b3){
            int pointX = (int) (e.getX() / 50) * 50;
            int pointY = (int) (e.getY() / 50) * 50;
            currentX = pointX / 50 + sideX;
            currentY = pointY / 50 + sideY;
            rightButtonAction();
            }
        }
    public void mouseMoved(MouseEvent e) {}
    HashMap<String, BufferedImage> tilesDemo = new HashMap<String, BufferedImage>();
    private String[][] mainArray;
    String newMapCoded;
    String addText = "1", secondText = "17";
    ArrayList<String> map = new ArrayList<String>();
    JPanel chose = new JPanel();
   public ImagePanel newMap;
    File mFile;
    public int prevColsX, prevRowsY;
    ArrayList<JButton> buttons = new ArrayList<JButton>();
    BufferedImage tempImage;
    BufferedImage image, currentImage, totalImage, mobImage, boxImage, playerImage, doorImage, entranceImage, exitImage;
    JScrollPane scrollPaneOne, scrollPaneTwo;
    Random r = new Random();
    int columnsX = prevColsX= 50, rowsY = prevRowsY =50;
    JButton ButtonIncreaseX = new JButton(">>");
    JButton ButtonDecreaseX = new JButton("<<");
    JButton ButtonIncreaseY = new JButton(">>");
    JButton ButtonDecreaseY = new JButton("<<");
    JLabel colsX = new JLabel("columns " + columnsX), rowY= new JLabel("rows " + rowsY);
    ActionListener AL1 = new TestActionListener();
    BasicArrowButton buttonDown = new BasicArrowButton(BasicArrowButton.SOUTH);
    BasicArrowButton buttonUp = new BasicArrowButton(BasicArrowButton.NORTH);
    BasicArrowButton buttonLeft = new BasicArrowButton(BasicArrowButton.WEST);
    BasicArrowButton buttonRight = new BasicArrowButton(BasicArrowButton.EAST);
    int sideX = 0 , sideY = 0;
    JButton save = new JButton("s");
    JButton load = new JButton("load Tile");
    JButton loadMap = new JButton("load Map");
    int currentX, currentY;
    int buttonNumber = 0;
    int tilesOnScreenX = 10, tilesOnScreenY = 13;
    private BufferedImage pixelMap;
    private String mPath;
    Border thickBorder;

    public void leftButtonAction(){
        if(currentY >= 0 && currentX >= 0 && currentX < columnsX && currentY < rowsY)
            System.out.println("addText " + addText);
            if(addText.equals("2")) {
                addText("/");
            } else if(addText.equals("26")){
                addText("*");
            }else if(addText.equals("27")){
                addText("p");
            } else if(addText.equals("28")){
                addText("d");
            } else if(addText.equals("29")){
                addText("+");
            } else if(addText.equals("30")){
                addText("-");
            }else
                mainArray[currentX][currentY] = addText;
        remakeMap();
    }
    public void rightButtonAction(){
        if(currentY >= 0 && currentX >= 0 && currentX < columnsX && currentY < rowsY)
            if(secondText.equals("2")) {
                addText("/");
            } else if(secondText.equals("26")){
                addText("*");
            } else if(secondText.equals("27")){
                addText("p");
            } else if(secondText.equals("28")){
                addText("d");
            } else if(secondText.equals("29")){
                addText("+");
            } else if(secondText.equals("30")){
                addText("-");
            }else
                mainArray[currentX][currentY] = secondText;
        remakeMap();
    }
    public void addText(String c){
        if (mainArray[currentX][currentY].matches("[0-9]+")){
            mainArray[currentX][currentY] = mainArray[currentX][currentY] + c;
        }else {
            mainArray[currentX][currentY] = mainArray[currentX][currentY].substring(0, mainArray[currentX][currentY].length()-1) + c;
        }
    }
    public MapCreator(String title) {
        super(title);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setLayout(null);

        save.setBackground(Color.gray);
        load.setBackground(Color.gray);
        loadMap.setBackground(Color.gray);
        ButtonIncreaseX.setBackground(Color.gray);
        ButtonDecreaseX.setBackground(Color.gray);
        ButtonIncreaseY.setBackground(Color.gray);
        ButtonDecreaseY.setBackground(Color.gray);
        buttonDown.setBackground(Color.gray);
        buttonUp.setBackground(Color.gray);
        buttonLeft.setBackground(Color.gray);
        buttonRight.setBackground(Color.gray);
        mainArray = new String[columnsX][rowsY];
        for(int x = 0; x < columnsX; x++){
            for(int y = 0; y < rowsY; y++){
                mainArray[x][y] = "17";
            }
        }

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        double width = screenSize.getWidth();
        double height = screenSize.getHeight();
        chose.setLayout(new GridLayout(15,2));
        chose.setBackground(Color.gray);
        scrollPaneOne = new JScrollPane(chose);
        scrollPaneOne.setBackground(Color.gray);
        scrollPaneOne.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPaneOne.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        ButtonDecreaseX.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                prevColsX = columnsX;
                columnsX--;
                colsX.setText("columns " + columnsX);
                upgradeMap();
            }
        });
        ButtonIncreaseX.addActionListener(new ActionListener() {@Override public void actionPerformed(ActionEvent e) {prevColsX =columnsX;columnsX++;colsX.setText("columns " + columnsX);upgradeMap();}});
        ButtonDecreaseY.addActionListener(new ActionListener() {@Override public void actionPerformed(ActionEvent e) {prevRowsY =rowsY; rowsY--;rowY.setText("rows " + rowsY);upgradeMap();}});
        ButtonIncreaseY.addActionListener(new ActionListener() {@Override public void actionPerformed(ActionEvent e) {prevRowsY =rowsY;rowsY++;rowY.setText("rows " + rowsY);upgradeMap();}});
        load.addActionListener(new OpenFileAction(new JFrame(), new JFileChooser()));
        loadMap.addActionListener(new OpenFileAction(new JFrame(), new JFileChooser()));
        chose.add(ButtonDecreaseX);
        chose.add(colsX);
        chose.add(ButtonIncreaseX);
        chose.add(ButtonDecreaseY);
        chose.add(rowY);
        chose.add(ButtonIncreaseY);
        chose.add(load);
        chose.add(loadMap);
        thickBorder = new LineBorder(null, 1);
        for(int i = 0; i <= 30; i++){
            final int x = i;
            buttons.add(new JButton(Integer.toString(i)));
            buttons.get(i).setBackground(Color.gray);
//            buttons.get(i).addActionListener(new ActionListener() {
//                public void actionPerformed(ActionEvent e) {
//
//                    addText = buttons.get(x).getText();
//                }
//            });
            buttons.get(i).addMouseListener(new MouseInputListener() {
                @Override
                public void mouseClicked(MouseEvent e) {

                }

                @Override
                public void mousePressed(MouseEvent e) {
                    if(e.getButton() == 1) {
                        addText = buttons.get(x).getText();
                    } else if(e.getButton() == 3) {
                        secondText = buttons.get(x).getText();
                    }
                }

                @Override
                public void mouseReleased(MouseEvent e) {

                }

                @Override
                public void mouseEntered(MouseEvent e) {

                }

                @Override
                public void mouseExited(MouseEvent e) {

                }

                @Override
                public void mouseDragged(MouseEvent e) {

                }

                @Override
                public void mouseMoved(MouseEvent e) {

                }
            });
            buttons.get(i).setBorder(thickBorder);
            buttons.get(i).setPreferredSize(new Dimension(120, 120));
            if(i > 0)
            chose.add(buttons.get(i));
            buttonNumber++;
        }
        buttonDown.addActionListener(new ActionListener() {public void actionPerformed(ActionEvent e) {sideY++; remakeMap();}});
        buttonUp.addActionListener(new ActionListener() {public void actionPerformed(ActionEvent e) {sideY--;if(sideY < 0)sideY = 0;remakeMap();}});
        buttonLeft.addActionListener(new ActionListener() {public void actionPerformed(ActionEvent e) {sideX--;if(sideX < 0)sideX = 0;remakeMap();}});
        buttonRight.addActionListener(new ActionListener() {public void actionPerformed(ActionEvent e) {sideX++;remakeMap();}});
        save.addActionListener(new SaveActionListener());

        try {
            image = ImageIO.read(this.getClass().getResource("/images/spritesheet.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            mobImage = ImageIO.read(this.getClass().getResource("/images/test.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            boxImage = ImageIO.read(this.getClass().getResource("/images/boxOpen.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            playerImage = ImageIO.read(this.getClass().getResource("/images/player.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            doorImage = ImageIO.read(this.getClass().getResource("/images/door.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            exitImage = ImageIO.read(this.getClass().getResource("/images/EXIT.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            entranceImage = ImageIO.read(this.getClass().getResource("/images/entrance.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        setPreferredSize(new Dimension((int)width, (int)(height*0.95f)));

        pack();
        addButtons();

        tilesOnScreenX = (int)(getWidth()*0.7f/50f);
        tilesOnScreenY = (int)(getHeight()*0.9f/50f);
        totalImage = new BufferedImage(1000, 1000, BufferedImage.TYPE_INT_ARGB);
        Graphics s = totalImage.createGraphics();
        for(int x = 0; x < tilesOnScreenX; x++){
            for(int y = 0; y < tilesOnScreenY; y++){
                if(x<tilesOnScreenX && y < tilesOnScreenY){
               String q = mainArray[x][y];

                        s.drawImage(tilesDemo.get(q),x*50,y*50,null);}
            }
        }

        newMap = new ImagePanel(totalImage);
        newMap.addMouseMotionListener(this);
        newMap.addMouseListener(new MouseInputListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
            }

            public void mousePressed(MouseEvent e) {
                if(e.getButton() == 1) {
                    int pointX = (int) (e.getX() / 50) * 50;
                    int pointY = (int) (e.getY() / 50) * 50;
                    currentX = pointX / 50 + sideX;
                    currentY = pointY / 50 + sideY;
                    leftButtonAction();
                }else if(e.getButton() == 3) {
                    int pointX = (int) (e.getX() / 50) * 50;
                    int pointY = (int) (e.getY() / 50) * 50;
                    currentX = pointX / 50 + sideX;
                    currentY = pointY / 50 + sideY;
                    rightButtonAction();
                }
            }

            public void mouseReleased(MouseEvent e) {

            }

            public void mouseEntered(MouseEvent e) {

            }

            public void mouseExited(MouseEvent e) {

            }

            public void mouseDragged(MouseEvent e) {

            }

            public void mouseMoved(MouseEvent e) {
            }
        });
        scrollPaneTwo = new JScrollPane(newMap);
        scrollPaneTwo.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPaneTwo.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);

        for(int y = 0; y < rowsY; y++){
            newMapCoded = null;
            if(newMapCoded != null){
            newMapCoded = newMapCoded + "/";}
            for(int x = 0; x < columnsX; x++){
                if(newMapCoded != null){
                    newMapCoded = newMapCoded+",";}
                else newMapCoded = ",";
            }
            map.add(newMapCoded);
        }
        scrollPaneTwo.setBounds((int)(getWidth()*0.25f), 0, tilesOnScreenX*50, tilesOnScreenY*50);

        buttonUp.setBounds((int)(scrollPaneTwo.getX()+scrollPaneTwo.getWidth()), 0,
                (int)(getWidth()-(scrollPaneTwo.getX()+scrollPaneTwo.getWidth())), (int)(scrollPaneTwo.getHeight()/2));

        buttonDown.setBounds((int)(scrollPaneTwo.getX()+scrollPaneTwo.getWidth()), (int)(scrollPaneTwo.getHeight()/2),
                (int)(getWidth()-(scrollPaneTwo.getX()+scrollPaneTwo.getWidth())),(int)(scrollPaneTwo.getHeight()/2));

        buttonLeft.setBounds((int)(getWidth()*0.25f), (int)(scrollPaneTwo.getY()+scrollPaneTwo.getHeight()),
                (int)(scrollPaneTwo.getWidth()/2), (int)(getHeight()-(scrollPaneTwo.getY()+scrollPaneTwo.getHeight())));
        buttonRight.setBounds((int)(getWidth()*0.25f)+(int)(scrollPaneTwo.getWidth()/2),  (int)(scrollPaneTwo.getY()+scrollPaneTwo.getHeight()),
                (int)(scrollPaneTwo.getWidth()/2),(int)(getHeight()-(scrollPaneTwo.getY()+scrollPaneTwo.getHeight())));
        save.setBounds((int)(scrollPaneTwo.getX()+scrollPaneTwo.getWidth()),(int)(scrollPaneTwo.getY()+scrollPaneTwo.getHeight()),
                (int)(getWidth()-(scrollPaneTwo.getX()+scrollPaneTwo.getWidth())),(int)(getHeight()-(scrollPaneTwo.getY()+scrollPaneTwo.getHeight())));

        scrollPaneOne.setBounds(0, 0, (int)(getWidth()*0.25f), (int)(getHeight()));

        add(scrollPaneOne);
        add(scrollPaneTwo);
        add(buttonDown);
        add(buttonUp);
        add(buttonLeft);
        add(buttonRight);
        add(save);
        pack();
        setVisible(true);
        setLocationRelativeTo(null);


    }
    public void upgradeMap(){
        if(columnsX < tilesOnScreenX) {
            columnsX = prevColsX;
            colsX.setText("columns " + columnsX);
        }
        if(rowsY < tilesOnScreenY){
            rowsY = prevRowsY;
            rowY.setText("rows " + rowsY);
        }
         String[][] tempArray = new String[columnsX][rowsY];
        for(int x = 0; x < columnsX; x++){
            for(int y = 0; y < rowsY; y++){
                if(x< prevColsX && y <prevRowsY){
                    tempArray[x][y] =  mainArray[x][y];}
                else{
                    tempArray[x][y] = "17";
                }

            }
        }
        mainArray = null;
        mainArray = tempArray;
        remakeMap();
    }
    public void remakeMap(){
        if(sideY > rowsY-tilesOnScreenY){
            sideY--;
        }
        if(sideX > columnsX-tilesOnScreenX){
            sideX--;}
        drawMap(sideX, sideY);

        newMap = new ImagePanel(totalImage);

        scrollPaneTwo = new JScrollPane(newMap);
        scrollPaneTwo.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPaneTwo.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);

        scrollPaneTwo.setBounds((int)(getWidth()*0.25f), 0, tilesOnScreenX*50, tilesOnScreenY*50);
        remove(scrollPaneTwo);
        add(scrollPaneTwo);

        revalidate();
    }


    public void addButtons(){
        buttons.get(0).setIcon(new ImageIcon(image.getSubimage(0, 0, 120, 120)));
        putPicture("0", image.getSubimage(1148, 334, 25, 25));

        buttons.get(1).setIcon(new ImageIcon(image.getSubimage(0, 0, 120, 120)));
        putPicture("1", image.getSubimage(0, 0, 120, 120));


        buttons.get(2).setIcon(new ImageIcon(mobImage.getSubimage(0, 0, 120, 120)));
        putPicture("2", mobImage.getSubimage(0,0,120,120));

        buttons.get(3).setIcon(new ImageIcon(image.getSubimage(0, 242, 120, 120)));
        putPicture("3", image.getSubimage(0,242,120,120));

        buttons.get(4).setIcon(new ImageIcon(image.getSubimage(0, 363, 120, 120)));
        putPicture("4", image.getSubimage(0,363,120,120));

        buttons.get(5).setIcon(new ImageIcon(image.getSubimage(0, 484, 120, 120)));
        putPicture("5", image.getSubimage(0,484,120,120));

        buttons.get(6).setIcon(new ImageIcon(image.getSubimage(250, 0, 120, 120)));
        putPicture("6", image.getSubimage(250,0,120,120));

        buttons.get(7).setIcon(new ImageIcon(image.getSubimage(250, 121, 120, 120)));
        putPicture("7", image.getSubimage(250,121,120,120));

        buttons.get(8).setIcon(new ImageIcon(image.getSubimage(250, 242, 120, 120)));
        putPicture("8", image.getSubimage(250,242,120,120));

        buttons.get(9).setIcon(new ImageIcon(image.getSubimage(250, 363, 120, 120)));
        putPicture("9", image.getSubimage(250,363,120,120));

        buttons.get(10).setIcon(new ImageIcon(image.getSubimage(250, 484, 120, 120)));
        putPicture("10", image.getSubimage(250,484,120,120));

        buttons.get(11).setIcon(new ImageIcon(image.getSubimage(500, 0, 120, 120)));
        putPicture("11", image.getSubimage(500,0,120,120));

        buttons.get(12).setIcon(new ImageIcon(image.getSubimage(500, 121, 120, 120)));
        putPicture("12", image.getSubimage(500,121,120,120));

        buttons.get(13).setIcon(new ImageIcon(image.getSubimage(500, 242, 120, 120)));
        putPicture("13", image.getSubimage(500,242,120,120));

        buttons.get(14).setIcon(new ImageIcon(image.getSubimage(500, 363, 120, 120)));
        putPicture("14", image.getSubimage(500,363,120,120));

        buttons.get(15).setIcon(new ImageIcon(image.getSubimage(500, 484, 120, 120)));
        putPicture("15", image.getSubimage(500,484,120,120));

        buttons.get(16).setIcon(new ImageIcon(image.getSubimage(750, 0, 120, 120)));
        putPicture("16", image.getSubimage(750,0,120,120));

        buttons.get(17).setIcon(new ImageIcon(image.getSubimage(750, 121, 120, 120)));
        putPicture("17", image.getSubimage(750,121,120,120));

        buttons.get(18).setIcon(new ImageIcon(image.getSubimage(750, 242, 120, 120)));
        putPicture("18", image.getSubimage(750,242,120,120));

        buttons.get(19).setIcon(new ImageIcon(image.getSubimage(750, 363, 120, 120)));
        putPicture("19", image.getSubimage(750,363,120,120));

        buttons.get(20).setIcon(new ImageIcon(image.getSubimage(750, 484, 120, 120)));
        putPicture("20", image.getSubimage(750,484,120,120));

        buttons.get(21).setIcon(new ImageIcon(image.getSubimage(1000,0,120,120)));
        putPicture("21", image.getSubimage(1000,0,120,120));

        buttons.get(22).setIcon(new ImageIcon(image.getSubimage(1000, 121, 120, 120)));
        putPicture("22", image.getSubimage(1000,121,120,120));

        buttons.get(23).setIcon(new ImageIcon(image.getSubimage(1000,242,120,120)));
        putPicture("23", image.getSubimage(1000,242,120,120));

        buttons.get(24).setIcon(new ImageIcon(image.getSubimage(1000, 363, 120, 120)));
        putPicture("24", image.getSubimage(1000,363,120,120));

        buttons.get(25).setIcon(new ImageIcon(image.getSubimage(1000, 484, 120, 120)));
        putPicture("25", image.getSubimage(1000,484,120,120));

        buttons.get(26).setIcon(new ImageIcon(boxImage));
        putPicture("26", boxImage);

        buttons.get(27).setIcon(new ImageIcon(playerImage));
        putPicture("27", playerImage);

        buttons.get(28).setIcon(new ImageIcon(doorImage));
        putPicture("28", doorImage);

        buttons.get(29).setIcon(new ImageIcon(entranceImage));
        putPicture("29", entranceImage);

        buttons.get(30).setIcon(new ImageIcon(exitImage));
        putPicture("30", exitImage);

    }

    public class OpenFileAction extends AbstractAction {
        JFrame frame;
        JFileChooser chooser;

        OpenFileAction(JFrame frame, JFileChooser chooser) {
            super("Open...");
            this.chooser = chooser;
            this.frame = frame;
        }
        public void actionPerformed(ActionEvent evt) {
            // Show dialog; this method does not return until dialog is closed
            chooser.showOpenDialog(frame);

            // Get the selected file
            mFile = chooser.getSelectedFile();

            if(evt.getSource() == load){
                try {
                    tempImage = ImageIO.read(mFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                final int x = buttonNumber;
            buttons.add(new JButton(new ImageIcon(tempImage.getSubimage(0, 0, 120, 120))));
            buttons.get(buttonNumber).setText(Integer.toString(buttonNumber));
//            buttons.get(buttonNumber).addActionListener(new ActionListener() {
//                public void actionPerformed(ActionEvent e) {
//                    addText = buttons.get(x).getText();
//                }
//            });
                buttons.get(buttonNumber).addMouseListener(new MouseInputListener() {
                    @Override
                    public void mouseClicked(MouseEvent e) {

                    }

                    @Override
                    public void mousePressed(MouseEvent e) {
                        if(e.getButton() == 1) {
                            addText = buttons.get(x).getText();
                        } else if(e.getButton() == 3) {
                            secondText = buttons.get(x).getText();
                        }
                    }

                    @Override
                    public void mouseReleased(MouseEvent e) {

                    }

                    @Override
                    public void mouseEntered(MouseEvent e) {

                    }

                    @Override
                    public void mouseExited(MouseEvent e) {

                    }

                    @Override
                    public void mouseDragged(MouseEvent e) {

                    }

                    @Override
                    public void mouseMoved(MouseEvent e) {

                    }
                });
            buttons.get(buttonNumber).setBackground(Color.gray);
            buttons.get(buttonNumber).setBorder(thickBorder);
            buttons.get(buttonNumber).setPreferredSize(new Dimension(120, 120));
            chose.add(buttons.get(buttonNumber));
            revalidate();
            putPicture("" + buttonNumber, tempImage.getSubimage(0, 0, 120, 120));
            buttonNumber++;}
            if(evt.getSource() == loadMap){
                loadMapFromFile(mFile);
            }
        }
    }
    public class TestActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            Icon icon = ((JButton)e.getSource()).getIcon();
            currentImage = new BufferedImage(
                    icon.getIconWidth(),
                    icon.getIconHeight(),
                    BufferedImage.TYPE_INT_RGB);
            Graphics g = currentImage.createGraphics();
// paint the Icon to the BufferedImage.
            icon.paintIcon(null, g, 0,0);
            g.dispose();

            Image tmp = currentImage.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
            BufferedImage dimg = new BufferedImage(50, 50, BufferedImage.TYPE_INT_ARGB);

            Graphics2D g2d = dimg.createGraphics();
            g2d.drawImage(tmp, 0, 0, null);
            g2d.dispose();

            currentImage =  dimg;


        }

    }
    public class SaveActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            int walls = new Color(0,0,0).getRGB();
            int floor = new Color(255,255,255).getRGB();
            BufferedImage pixelMap = new BufferedImage(columnsX, rowsY, BufferedImage.TYPE_INT_RGB);
            for (int x =0; x < columnsX; x++){
                for (int y =0; y < rowsY; y++){

                    if(mainArray[x][y].equals("1") || mainArray[x][y].equals("1/"))
                    pixelMap.setRGB(x, y, floor);
                    else
                    pixelMap.setRGB(x, y, walls);
                }
            }

            try {
                map.clear();
                for(int x = 0; x < rowsY; x++){
                    String row = "";
                    for(int y = 0; y < columnsX; y++){
                        row = row+mainArray[y][x]+",";
                    }
                    map.add(row);
                }



                FileWriter fw = new FileWriter( actionPerformed2() + ".txt");
                BufferedWriter bw = new BufferedWriter(fw);

                for(int i = 0; i <map.size(); i++){
                    if(i != 0)
                    bw.newLine();
                    bw.write(map.get(i));
                }
                bw.close();
                File outputFile = new File(mPath + ".png");
                ImageIO.write(pixelMap, "png", outputFile);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

        public File actionPerformed2() {
            FileDialog fDialog = new FileDialog(this, "Save", FileDialog.SAVE);
            fDialog.setVisible(true);
            mPath = fDialog.getDirectory() + fDialog.getFile();
            File f = new File(mPath);
            return f;
            // f.createNewFile(); etc.
    }
    public void loadMapFromFile(File f){
        String everything;
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(f));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            StringBuilder sb = new StringBuilder();
            String line = null;
            try {
                line = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                try {
                    line = br.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            everything = sb.toString();
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        uncodeMap(everything);
    }
    public void uncodeMap(String s){
        ArrayList<String> string = new ArrayList<String>();
        ArrayList<Integer> newRows = new ArrayList<Integer>();
        char [] bobik = s.toCharArray();
        for (int i=0; i<bobik.length; i++) {
            if (bobik[i] == '\n') {
                newRows.add(i);
            }
        }
        int start = 0;
        int end = 0;
        for (int i=0; i<newRows.size(); i++) {
            end = newRows.get(i);
            string.add(s.substring(start, end));
            start = newRows.get(i)+1;
        }
        ArrayList<Integer> testRow2 = new ArrayList<Integer>();
        char [] testRow = string.get(0).toCharArray();
        for (int i=0; i<testRow.length; i++) {
            if (testRow[i] == ',') {
                testRow2.add(i);
            }
        }
         String[][] tempArray = new String[testRow2.size()][newRows.size()];
        for(int y = 0; y < newRows.size(); y++){
            char [] row = string.get(y).toCharArray();
            ArrayList<Integer> row2 = new ArrayList<Integer>();
            for (int e=0; e<row.length; e++) {
                if (row[e] == ',') {
                    row2.add(e);
                }
            }
            int start2 = 0;
            int end2 = 0;
            for(int x = 0; x < row2.size(); x++){
                end2 = row2.get(x);
                tempArray[x][y] = string.get(y).substring(start2, end2);
                start2 = row2.get(x)+1;
            }
        }
        mainArray = tempArray;
        columnsX = testRow2.size();
        rowsY = newRows.size();
        colsX.setText("columns " + columnsX);
        rowY.setText("rows " + rowsY);
        remakeMap();
    }

class ImagePanel extends JPanel {

    private Image img;

    public ImagePanel(Image img) {
        this.img = img;
        Dimension size = new Dimension(img.getWidth(null), img.getHeight(null));
        setPreferredSize(size);
        setMinimumSize(size);
        setMaximumSize(size);
        setSize(size);
        setLayout(null);
    }

    public void setImg(Image img) {
        this.img = img;
    }

    public void paintComponent(Graphics g) {
        g.drawImage(img, 0, 0, null);
    }

}
public void putPicture(String i, Image img){
    Image tmp = img.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
    BufferedImage dimg = new BufferedImage(50, 50, BufferedImage.TYPE_INT_ARGB);

    Graphics2D g2d = dimg.createGraphics();
    g2d.drawImage(tmp, 0, 0, null);
    g2d.dispose();
    tilesDemo.put(i, dimg);
}


public void drawMap(int X, int Y){
//    totalImage = new BufferedImage(515, 650, BufferedImage.TYPE_INT_ARGB);
    Graphics s = totalImage.createGraphics();
    int drawX = -50;
    int drawY = 0;
    int width = (int)Math.ceil(515/50)+X;
    for(int x = X; x < tilesOnScreenX+X; x++){
        drawX+=50;
        drawY = 0;
        for(int y = Y; y < tilesOnScreenY+Y; y++){
            if(x < columnsX && y < rowsY){
                String q = mainArray[x][y];
if(q.contains("/")){
    q = q.substring(0,q.length()-1);
    s.drawImage(tilesDemo.get(q),drawX,drawY,null);
    s.drawImage(tilesDemo.get("2"),drawX,drawY,null);
} else if(q.contains("*")){
    q = q.substring(0,q.length()-1);
    s.drawImage(tilesDemo.get(q),drawX,drawY,null);
    s.drawImage(tilesDemo.get("26"),drawX,drawY,null);
} else if(q.contains("p")){
    q = q.substring(0,q.length()-1);
    s.drawImage(tilesDemo.get(q),drawX,drawY,null);
    s.drawImage(tilesDemo.get("27"),drawX,drawY,null);
} else if(q.contains("d")){
    q = q.substring(0,q.length()-1);
    s.drawImage(tilesDemo.get(q),drawX,drawY,null);
    s.drawImage(tilesDemo.get("28"),drawX,drawY,null);
} else if(q.contains("+")){
    q = q.substring(0,q.length()-1);
    s.drawImage(tilesDemo.get(q),drawX,drawY,null);
    s.drawImage(tilesDemo.get("29"),drawX,drawY,null);
} else if(q.contains("-")){
    q = q.substring(0,q.length()-1);
    s.drawImage(tilesDemo.get(q),drawX,drawY,null);
    s.drawImage(tilesDemo.get("30"),drawX,drawY,null);
} else
            s.drawImage(tilesDemo.get(q),drawX,drawY,null);
            drawY+=50;}
        }
    }
    s.dispose();
}
}